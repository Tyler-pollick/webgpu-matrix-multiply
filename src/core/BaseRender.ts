export class BaseRenderer {

    public canvas: HTMLCanvasElement;

    public maxFrameRate = 100;

    private animationFrameId: number;
    private lastTimestamp: DOMHighResTimeStamp = -1;
    private frameCount: number = -1;

    constructor(canvas) {
        this.canvas = canvas;
    }

    start() {
        window.addEventListener('resize', this.resizeCallback.bind(this));
        this.resizeCallback();
        this.animationFrameId = requestAnimationFrame(this.frameCallback.bind(this));
    }

    stop() {
        if (this.animationFrameId) {
            cancelAnimationFrame(this.animationFrameId);
            this.animationFrameId = 0;
        }
        window.removeEventListener('resize', this.resizeCallback.bind(this));
    }

    protected onResize(width: number, height: number) {
        // Override me
    }

    protected beforeFrame(timestamp: DOMHighResTimeStamp, timeDelta: number) {
        // Override me
    }

    protected onFrame(timestamp: DOMHighResTimeStamp, timeDelta: number) {
        // Override me
    }

    private resizeCallback() {
        this.onResize(this.canvas.width, this.canvas.height);
    }

    private frameCallback(timestamp: DOMHighResTimeStamp) {
        const timeDelta = this.lastTimestamp === -1 ? 0 : timestamp - this.lastTimestamp;
        this.lastTimestamp = timestamp;

        this.animationFrameId = requestAnimationFrame(this.frameCallback.bind(this));

        this.frameCount++;
        if (this.frameCount % this.maxFrameRate === 0) {
            return;
        }

        this.beforeFrame(timestamp, timeDelta);

        this.onFrame(timestamp, timeDelta);
    }
}