export enum VertexShaderInputType {
    Custom = 0,
    VertexShaderInputBase = 1,
    VertexShaderInputTan = 2
}

export type ShaderAttributeDefinition = {
    name: string,
    format: GPUVertexFormat
    location: number
}
export type ShaderAttributeDefinitions = Record<string, ShaderAttributeDefinition>;

export type ShaderUniformDefinition = {
    name: string,
    binding: number,
}

export type ShaderModule = {
    attributeType: VertexShaderInputType,
    attributes?: ShaderAttributeDefinition[] 
    uniforms?: ShaderUniformDefinition[],
    vMain?: string | 'v_main';
    fMain?: string | 'f_main';
    code: string
}

export const VertexShaderInput = {
    [VertexShaderInputType.VertexShaderInputBase]: <ShaderAttributeDefinition[]>[
        {
            name: 'vertices',
            format: 'float32x3',
            location: 0
        },
        // {
        //     name: 'normal',
        //     format: 'float32x3',
        //     location: 1
        // },
        {
            name: 'color',
            format: 'float32x3',
            location: 1
        }
    ],
    [VertexShaderInputType.VertexShaderInputTan]: <ShaderAttributeDefinition[]>[
        {
            name: 'position',
            format: 'float32x3',
            location: 0
        },
        {
            name: 'normal',
            format: 'float32x3',
            location: 1
        }
    ]
}

export const CommonFragments = {
    Inputs: /*wgsl*/`
        struct VertexShaderInputBase {
            @location(0) position: vec3<f32>,
            // @location(1) normal: vec3<f32>,
            @location(1) color: vec3<f32>,
        }
        struct VertexShaderInputTan {
            @location(0) position: vec3<f32>,
            @location(1) normal: vec3<f32>,
        }
    `,
};

