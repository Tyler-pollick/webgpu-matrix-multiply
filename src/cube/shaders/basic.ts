import {
    VertexShaderInputType,
    CommonFragments,
    ShaderModule
} from './common';

const shader = <ShaderModule> {
    attributeType: VertexShaderInputType.VertexShaderInputBase,
    code: /*wgsl*/`
        ${CommonFragments.Inputs};
        
        struct VertexOut {
            @builtin(position) position: vec4<f32>,
            @location(0) color: vec3<f32>
        }
        
        @vertex
        fn v_main(vertexData: VertexShaderInputBase) -> VertexOut 
        {
            var output: VertexOut;
            output.position = vec4<f32>(vertexData.position, 1.0);
            output.color = vertexData.color;
            return output;
        }
        
        @fragment
        fn f_main(@location(0) color: vec3<f32>) -> @location(0) vec4<f32>
        {
            return vec4<f32>(color, 1.0);
        }
    `
}
export default shader