import { getDevice } from "../utils";
import { vec3, quat, mat4 } from "gl-matrix";
import { BaseRenderer } from '../core/BaseRender';
import BasicShader from "./shaders/basic";
import { ShaderModule, VertexShaderInput, VertexShaderInputType } from './shaders/common';
import { vec4 } from "gl-matrix";

type BufferArrayFormat = Uint16Array | Uint32Array | Float32Array;
function createBuffer(device: GPUDevice, array: BufferArrayFormat, usage: GPUBufferUsageFlags = GPUBufferUsage.COPY_DST) {
    const buffer = device.createBuffer({
        size: (array.byteLength + 3) & ~3,
        usage,
        mappedAtCreation: true
    });
    const arrayBuffer = buffer.getMappedRange();
    if (array instanceof Uint16Array) new Uint16Array(arrayBuffer).set(array);
    else if (array instanceof Uint32Array) new Uint32Array(arrayBuffer).set(array);
    else if (array instanceof Float32Array) new Float32Array(arrayBuffer).set(array);
    buffer.unmap();
    return buffer;
};

class BufferAttribute<T extends BufferArrayFormat> {

    array: T;
    itemSize: number;
    itemCount: number;

    usage: GPUBufferUsageFlags;

    constructor(array: T, itemSize: number, usage: GPUBufferUsageFlags = GPUBufferUsage.COPY_DST) {
        this.array = array;
        this.itemSize = itemSize;
        this.itemCount = this.array.length / this.itemSize;

        this.usage = usage;
    }

    // TODO: Add utility stuff
}

class BufferGeometry {

    vertices?: BufferAttribute<BufferArrayFormat>;
    color?: BufferAttribute<BufferArrayFormat>;
    indices?: BufferAttribute<Uint16Array>;
    normals?: BufferAttribute<BufferArrayFormat>;

    // TODO: Add utility stuff
    computeNormals() {

        if (!this.normals) {



        }

    }
}

class Material {

}

const Axis = {
    x: vec3.clone([1, 0, 0]),
    y: vec3.clone([0, 1, 0]),
    z: vec3.clone([0, 0, 1])
};

class Object3D {

    position: vec3;
    rotation: vec3;
    quaternion: quat;
    scale: vec3;

    matrix: mat4;

    constructor() {
        this.position = vec3.clone([0, 0, 0]);
        this.rotation = vec3.clone([0, 0, 0]);
        this.quaternion = quat.clone([0, 0, 0, 1]);
        this.scale = vec3.clone([1, 1, 1]);

        this.matrix = mat4.create();
    }

    move(position: vec3) {
        vec3.copy(this.position, position);
    }

    translate(translation: vec3) {
        vec3.add(this.position, this.position, translation);
    }

    translateAxis(axis: vec3, delta) {
        let translation = vec3.create();
        vec3.scale(translation, axis, delta);
        this.translate(translation);
    }

    translateX(dx: number) {
        this.translateAxis(Axis.x, dx);
    }

    translateY(dy: number) {
        this.translateAxis(Axis.y, dy);
    }

    translateZ(dz: number) {
        this.translateAxis(Axis.z, dz);
    }

    rotate(rotation: vec3) {
        let quaternion = quat.create();
        quat.fromEuler(quaternion, rotation[0], rotation[1], rotation[2]);
        quat.add(this.quaternion, this.quaternion, quaternion);
    }

    rotateAxis(axis: vec3, rad: number) {
        let rotation = vec3.create();
        vec3.scale(rotation, axis, rad);
        this.rotate(rotation);
    }

    rotateX(rad: number) {
        this.rotateAxis(Axis.x, rad);
    }

    rotateY(rad: number) {
        this.rotateAxis(Axis.y, rad);
    }

    rotateZ(rad: number) {
        this.rotateAxis(Axis.z, rad);
    }

    updateMatrix() {
        mat4.fromRotationTranslationScale(
            this.matrix,
            this.quaternion,
            this.position,
            this.scale
        );
    }
}


class Shader {

    module: ShaderModule;

    constructor(module: ShaderModule) {
        this.module = module;
    }

    get attributes() {
        if (this.module.attributeType === VertexShaderInputType.Custom) {
            return this.module.attributes;
        }
        return VertexShaderInput[this.module.attributeType];
    }
}

class ShaderMaterial extends Material {

    shader: Shader;

    constructor(shader: Shader) {
        super();
        this.shader = shader;
    }
}

class Mesh extends Object3D {

    geometry: BufferGeometry;
    material: Material;

    render(passEncoder: GPURenderPassEncoder) {

    }
}

class Pipeline {
    device: GPUDevice;
    private settings: any;

    pipeline: GPURenderPipeline;
    attributes: { location: number, buffer: GPUBuffer }[];
    uniforms: { location: number, buffer: GPUBuffer }[];
    indexBuffer: GPUBuffer;

    constructor(device: GPUDevice, opts: any) {
        this.device = device;
        this.settings = opts;

        this.attributes = [];
        this.uniforms = [];
    }

    init(object: Mesh) {
        const geometry = object.geometry;
        const material = object.material;

        if (material instanceof ShaderMaterial) {
            const attributeBufferLayouts: GPUVertexBufferLayout[] = [];
            for (const attribute of material.shader.attributes) {

                attributeBufferLayouts.push({
                    attributes: [
                        {
                            shaderLocation: attribute.location,
                            offset: 0,
                            format: attribute.format
                        }
                    ],
                    arrayStride: 12,
                    stepMode: 'vertex'
                });

                this.attributes.push({
                    location: attribute.location,
                    buffer: createBuffer(this.device, geometry[attribute.name].array, geometry[attribute.name].usage)
                });
            }

            this.indexBuffer = createBuffer(this.device, geometry.indices.array, geometry.indices.usage);

            const module = this.device.createShaderModule({ code: material.shader.module.code });
            this.pipeline = this.device.createRenderPipeline({
                layout: 'auto',
                vertex: {
                    module,
                    entryPoint: material.shader.module.fMain ?? 'v_main',
                    buffers: attributeBufferLayouts
                },
                fragment: {
                    module,
                    entryPoint: material.shader.module.vMain ?? 'f_main',
                    targets: [
                        { format: this.settings.format }
                    ]
                },
                primitive: {
                    frontFace: 'cw',
                    cullMode: 'none',
                    topology: 'triangle-list'
                }
            });

        }
        else {
            throw "Materials other than ShaderMaterial are not supported";
        }
    }
}

class WebGpuRenderer extends BaseRenderer {

    // WebGPU Data Structures
    public adapter?: GPUAdapter;
    public device?: GPUDevice;

    // Frame Backings
    public context?: GPUCanvasContext;
    public colorTexture: GPUTexture;
    public colorTextureView: GPUTextureView;
    public depthTexture: GPUTexture;
    public depthTextureView: GPUTextureView;

    // public pipeline: GPURenderPipeline;

    public objects: Mesh[];
    public pipelines: WeakMap<Object3D, Pipeline>;

    constructor(canvas: HTMLCanvasElement, objects: Mesh[]) {
        super(canvas);
        this.objects = objects;
    }

    async init() {
        const {
            adapter,
            device
        } = await getDevice();

        this.adapter = adapter;
        this.device = device;
        this.context = this.canvas.getContext('webgpu');
        this.pipelines = new WeakMap();

        const presentationFormat = navigator.gpu.getPreferredCanvasFormat()
        this.context.configure({
            device: this.device,
            format: presentationFormat,
            usage: GPUTextureUsage.RENDER_ATTACHMENT | GPUTextureUsage.COPY_SRC,
            alphaMode: 'opaque'
        });

        this.depthTexture = device.createTexture({
            size: [this.canvas.width, this.canvas.height, 1],
            dimension: '2d',
            format: 'depth24plus',
            usage: GPUTextureUsage.RENDER_ATTACHMENT | GPUTextureUsage.COPY_SRC,
        });
        this.depthTextureView = this.depthTexture.createView();

        this.colorTexture = this.context.getCurrentTexture();
        this.colorTextureView = this.colorTexture.createView();

        const renderSettings = {
            format: presentationFormat
        }

        for (const object of this.objects) {
            const pipeline = new Pipeline(this.device, renderSettings);
            pipeline.init(object);
            this.pipelines.set(object, pipeline);
        }
    }

    protected onFrame(timestamp: number, timeDelta: number): void {
        console.log('create frame')
        
        const commandEncoder = this.device.createCommandEncoder();

        this.colorTexture = this.context.getCurrentTexture();
        this.colorTextureView = this.colorTexture.createView();

        const passEncoder = commandEncoder.beginRenderPass({
            colorAttachments: [
                {
                    view: this.colorTextureView,
                    clearValue: { r: 0.0, g: 0.0, b: 0.0, a: 1.0 },
                    loadOp: 'clear',
                    storeOp: 'store'
                }
            ]
        });

        for (const object of this.objects) {
            const pipeline = this.pipelines.get(object);

            passEncoder.setPipeline(pipeline.pipeline);
            passEncoder.setViewport(0, 0, this.canvas.width, this.canvas.height, 0, 1);
            passEncoder.setScissorRect(0, 0, this.canvas.width, this.canvas.height);

            for (const attribute of pipeline.attributes) {
                passEncoder.setVertexBuffer(attribute.location, attribute.buffer);
            }
            
            passEncoder.setIndexBuffer(pipeline.indexBuffer, 'uint16');
            passEncoder.drawIndexed(object.geometry.indices.itemCount);
        }

        passEncoder.end();

        this.device.queue.submit([ 
            commandEncoder.finish()
        ]);
    }
}

export async function main() {
    const canvasID = 'target';
    const canvas = document.getElementById(canvasID) as HTMLCanvasElement;
    if (!canvas) {
        throw new Error("Target node not defined");
    }

    const vertices = new Float32Array([
        -1.0, 1.0, 0.0,
        1.0, 1.0, 0.0,
        1.0, -1.0, 0.0,
        -1.0, -1.0, 0.0,
    ]);

    const colors = new Float32Array([
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,
        1.0, 0.0, 0.0
    ]);

    const indices = new Uint16Array([0, 1, 3, 1, 2, 3]);

    let geometry = new BufferGeometry();
    geometry.vertices = new BufferAttribute(vertices, 3, GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST);
    geometry.color = new BufferAttribute(colors, 3, GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST);
    geometry.indices = new BufferAttribute(indices, 1, GPUBufferUsage.INDEX);

    let shader = new Shader(BasicShader);
    let material = new ShaderMaterial(shader);

    let meshA = new Mesh();
    meshA.geometry = geometry;
    meshA.material = material;

    //
    // Object 2
    const vertices2 = new Float32Array([
        -1.0, 1.0, 0.0,
        1.0, 1.0, 0.0,
        0.0, -1.0, 0.0
    ]);

    const colors2 = new Float32Array([
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,
    ]);

    const indices2 = new Uint16Array([0, 1, 2]);

    let geometry2 = new BufferGeometry();
    geometry2.vertices = new BufferAttribute(vertices2, 3, GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST);
    geometry2.color = new BufferAttribute(colors2, 3, GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST);
    geometry2.indices = new BufferAttribute(indices2, 1, GPUBufferUsage.INDEX);

    let shader2 = new Shader(BasicShader);
    let material2 = new ShaderMaterial(shader2);

    let meshB = new Mesh();
    meshB.geometry = geometry2;
    meshB.material = material2;

    

    const renderer = new WebGpuRenderer(
        canvas, 
        [
            meshA,
            // meshB
        ]
    );

    console.log('here :)')

    await renderer.init();
    renderer.start();
}