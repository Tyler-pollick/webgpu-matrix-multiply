export const ATTRIB_MAP = {
    POSITION: 0,
    COLOR: 1
};

const VertexIn = /*wgsl*/`
    struct VertexIn {
        @location(${ATTRIB_MAP.POSITION}) position: vec3<f32>,
        @location(${ATTRIB_MAP.COLOR}) color: vec3<f32>
    };
`

const VertexOut = /*wgsl*/`
    struct VertexOut {
        @builtin(position) position: vec4<f32>,
        @location(0) color: vec3<f32>
    };
`;

export const BasicVertexSource = /*wgsl*/`
    ${VertexIn}
    ${VertexOut}

    @vertex
    fn main(vertexData: VertexIn) -> VertexOut 
    {
        var output: VertexOut;
        output.position = vec4<f32>(vertexData.position, 1.0);
        output.color = vertexData.color;
        return output;
    }
`;

export const BasicFragmentSource = /*wgsl*/`
    @fragment
    fn main(@location(0) color: vec3<f32>) -> @location(0) vec4<f32>
    {
        return vec4<f32>(color, 1.0);
    }
`;