import { getDevice } from "../utils";
import { BaseRenderer } from '../core/BaseRender';
import { 
    ATTRIB_MAP, 
    BasicVertexSource, 
    BasicFragmentSource 
} from "./shaders/basic";

type BufferArrayFormat = Uint16Array | Uint32Array | Float32Array;
function createBuffer(device: GPUDevice, array: BufferArrayFormat, usage: GPUBufferUsageFlags) {
    const buffer = device.createBuffer({
        size: (array.byteLength + 3) & ~3,
        usage,
        mappedAtCreation: true
    });
    const arrayBuffer = buffer.getMappedRange();
    if (array instanceof Uint16Array) new Uint16Array(arrayBuffer).set(array);
    else if (array instanceof Uint32Array) new Uint32Array(arrayBuffer).set(array);
    else if (array instanceof Float32Array) new Float32Array(arrayBuffer).set(array);
    buffer.unmap();
    return buffer;
}

class WebGpuRenderer extends BaseRenderer {

    // WebGPU Data Structures
    public adapter?: GPUAdapter;
    public device?: GPUDevice;

    // Frame Backings
    public context?: GPUCanvasContext;
    public colorTexture: GPUTexture;
    public colorTextureView: GPUTextureView;
    public depthTexture: GPUTexture;
    public depthTextureView: GPUTextureView;

    vertices: Float32Array;
    colors: Float32Array;
    indices: Uint16Array;

    private vertexBuffer: GPUBuffer;
    private colorBuffer: GPUBuffer;
    private indexBuffer: GPUBuffer;

    private uniformBindGroupLayout: GPUBindGroupLayout;
    private uniformBindGroup: GPUBindGroup;

    public pipeline: GPURenderPipeline;
    angle: number;
    first: any;

    constructor(canvas: HTMLCanvasElement) {
        super(canvas);
    }

    async init() {
        const {
            adapter,
            device
        } = await getDevice();

        this.adapter = adapter;
        this.device = device;
        this.context = this.canvas.getContext('webgpu');

        const presentationFormat = navigator.gpu.getPreferredCanvasFormat()
        this.context.configure({
            device: this.device,
            format: presentationFormat,
            usage: GPUTextureUsage.RENDER_ATTACHMENT | GPUTextureUsage.COPY_SRC,
            alphaMode: 'opaque'
        });

        this.depthTexture = device.createTexture({
            size: [this.canvas.width, this.canvas.height, 1],
            dimension: '2d',
            format: 'depth24plus',
            usage: GPUTextureUsage.RENDER_ATTACHMENT | GPUTextureUsage.COPY_SRC,
        });
        this.depthTextureView = this.depthTexture.createView();

        this.colorTexture = this.context.getCurrentTexture();
        this.colorTextureView = this.colorTexture.createView();

        this.vertices = new Float32Array([
            0.0, 1.0, 0.0,
            1.0, -1.0, 0.0,
            -1.0, -1.0, 0.0,
        ]);

        this.colors = new Float32Array([
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0,
        ]);

        this.indices = new Uint16Array([0, 1, 2]);

        this.vertexBuffer = createBuffer(this.device, this.vertices, GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST);
        this.colorBuffer = createBuffer(this.device, this.colors, GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST);
        this.indexBuffer = createBuffer(this.device, this.indices, GPUBufferUsage.INDEX);

        // @location(0)
        const vertexBufferDescriptors: GPUVertexBufferLayout = {
            attributes: [
                {
                    shaderLocation: ATTRIB_MAP.POSITION,
                    offset: 0,
                    format: 'float32x3'
                }
            ],
            arrayStride: 12,
            stepMode: 'vertex'
        };

        // @location(1)
        const colorBufferDescriptor: GPUVertexBufferLayout = {
            attributes: [
                {
                    shaderLocation: ATTRIB_MAP.COLOR,
                    offset: 0,
                    format: 'float32x3'
                },
            ],
            arrayStride: 12,
            stepMode: 'vertex'
        };

        const vertexModule =  this.device.createShaderModule({code: BasicVertexSource});
        const fragmentModule = this.device.createShaderModule({code: BasicFragmentSource});

        this.pipeline = device.createRenderPipeline({
            layout: "auto",
            vertex: {
                module: vertexModule,
                entryPoint: 'main',
                buffers: [vertexBufferDescriptors, colorBufferDescriptor]
            },
            fragment: {
                module: fragmentModule,
                entryPoint: 'main',
                targets: [
                    { format: presentationFormat }
                ]
            },
            primitive: {
                frontFace: 'cw',
                cullMode: 'none',
                topology: 'triangle-list'
            }
        });
    }

    protected beforeFrame(timestamp: number, timeDelta: number): void {
        
        this.angle = Math.min(this.angle + 0.5, 360);

        const STRIDE = 3;
        for (let idx=0; idx < this.vertices.length; idx+STRIDE) {
            const x = this.vertices[idx];
            const y = this.vertices[idx+1];
            const z = this.vertices[idx+2];

            this.vertices[idx]   = x * Math.cos(this.angle) - y * Math.sin(this.angle);
            this.vertices[idx+1] = x * Math.sin(this.angle) + y * Math.cos(this.angle);
            this.vertices[idx+2] = z;
        }
    }

    protected onFrame(timestamp: number, timeDelta: number): void {
        
        const commandEncoder = this.device.createCommandEncoder();

        this.colorTexture = this.context.getCurrentTexture();
        this.colorTextureView = this.colorTexture.createView();

        const passEncoder = commandEncoder.beginRenderPass({
            colorAttachments: [
                {
                    view: this.colorTextureView,
                    clearValue: { r: 0.0, g: 0.0, b: 0.0, a: 1.0 },
                    loadOp: 'clear',
                    storeOp: 'store'
                }
            ]
        });

        passEncoder.setPipeline(this.pipeline);
        passEncoder.setViewport(0, 0, this.canvas.width, this.canvas.height, 0, 1);
        passEncoder.setScissorRect(0, 0, this.canvas.width, this.canvas.height);
        passEncoder.setVertexBuffer(ATTRIB_MAP.POSITION, this.vertexBuffer);
        passEncoder.setVertexBuffer(ATTRIB_MAP.COLOR, this.colorBuffer);        
        passEncoder.setIndexBuffer(this.indexBuffer, 'uint16');
        passEncoder.drawIndexed(3);
        passEncoder.end();

        this.device.queue.submit([ 
            commandEncoder.finish()
        ]);
    }
}


interface SlimeSimulationConfiguration {
    device: GPUDevice,
    context: GPUCanvasContext
}

class SlimeSimulation {
    
    public renderer: Renderer;

    private agentBuffer: Uint8Array;     

    constructor(renderer: Renderer) {
        this.renderer = renderer;
    }

}

export async function main() {
    const canvasID = 'target';
    const canvas = document.getElementById(canvasID) as HTMLCanvasElement;
    if (!canvas) {
        throw new Error("Target node not defined");
    }

    const renderer = new WebGpuRenderer(canvas);
    const simulator = new SlimeSimulation(renderer);

    await renderer.init();
    renderer.start();
}