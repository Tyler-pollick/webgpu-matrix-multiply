/**
 * 
 * @returns 
 */
export async function getDevice() {
    const adapter = await navigator.gpu.requestAdapter();
    if (!adapter) {
        // console.error('Could not acquire WebGPU adapter.');
        // return;
        throw new Error('Could not acquire WebGPU adapter')
    }    
    const device = await adapter.requestDevice();
    return { adapter, device };
}

/**
 * 
 * @param nodeID 
 * @returns 
 */
export async function setupContext(nodeID: string) {
    const canvas = document.getElementById(nodeID) as HTMLCanvasElement;

    if (!canvas) {
        throw new Error("Target node not defined");
    }

    const context = canvas.getContext('webgpu');
    const {
        device
    } = await getDevice();


    context.configure({
        device,
        format: 'bgra8unorm',
        usage: GPUTextureUsage.RENDER_ATTACHMENT | GPUTextureUsage.COPY_SRC,
        alphaMode: 'opaque'
    });

    return { device, canvas, context };
}

/**
 * 
 * @param device 
 * @param url 
 * @returns 
 */
export async function loadShaderModule(device: GPUDevice, url: string) {
    const response = await fetch(url);
    const codeText = await response.text();
    const shaderModule = device.createShaderModule({
        code: codeText
    });
    return shaderModule;
}
